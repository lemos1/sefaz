## Installation

First you need to create virtualenv.
```sh
virtualenv --python=python3 env
```

Second you need to install all the dependencies required.
```sh
pip install -r requirements.txt
```


## GUIDE

 - Run the application by command
```sh
scrapy crawl sefaz -o data.json
```
 - See the output of your request in the directory "sefaz/data.json"

 - If you want to change your request, go to "sefaz/sefaz/spiders/sefaz_spider.py" and modify "formdata = {'CPF': '33608741020'}"