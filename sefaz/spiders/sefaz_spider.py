import scrapy
from sefaz.items import Item
from scrapy.http import FormRequest


class SefazSpider(scrapy.Spider):
    name = 'sefaz'
    start_urls = ['http://www.sefaz.ba.gov.br/scripts/cadastro/cadastroBa/consultaBa.asp']

    def parse(self, response):
        yield scrapy.FormRequest.from_response(response, 
            formdata={'CPF': '33608741020'},
            clickdata={"name":"B3", "value" :"CPF ->"},
            callback=self.after_form)

    def after_form(self, response):
        item = Item()
        item["cpf"] = [item.strip() for item in response.xpath('//table[@id="Table5"]//td[b="CPF:"]/text()').extract()]
        item["inscricao_estadual"] = [item.strip() for item in response.xpath('//td[b="Inscrição Estadual:"]/text()').extract()]
        item["razao_social"] = [item.strip() for item in response.xpath('//td[b="Razão Social:"]/text()').extract()]
        item["nome_fantasia"] = [item.strip() for item in response.xpath('//td[b="Nome Fantasia:"]/text()').extract()]
        item["unidade_atendimento"] = [item.strip() for item in response.xpath('//td[b="Unidade de Atendimento:"]/text()').extract()]
        item["unidade_fiscalizacao"] = [item.strip() for item in response.xpath('//td[b="Unidade de Fiscalização:"]/text()').extract()]
        item["logradouro"] = [item.strip() for item in response.xpath('//table[@id="Table6"]//td[b="Logradouro:"]/text()').extract()]
        item["numero"] = [item.strip() for item in response.xpath('//table[@id="Table6"]//td[b="Número:"]/text()').extract()]
        item["complemento"] = [item.strip() for item in response.xpath('//table[@id="Table6"]//td[b="Complemento:"]/text()').extract()]
        item["bairro_distrito"] = [item.strip() for item in response.xpath('//td[b="Bairro/Distrito:"]/text()').extract()]
        item["cep"] = [item.strip() for item in response.xpath('//table[@id="Table6"]//td[b="CEP:"]/text()').extract()]
        item["municipio"] = [item.strip() for item in response.xpath('//table[@id="Table6"]//td[b="Município:"]/text()').extract()]
        item["uf"] = [item.strip() for item in response.xpath('//table[@id="Table6"]//td[b="UF:"]/text()').extract()]
        item["telefone"] = [item.strip() for item in response.xpath('//td[b="Telefone:"]/text()').extract()]
        item["email"] = [item.strip() for item in response.xpath('//td[b="E-mail:"]/text()').extract()]
        item["referencia"] = [item.strip() for item in response.xpath('//table[@id="Table6"]//td[b="Referência:"]/text()').extract()]
        item["localizacao"] = [item.strip() for item in response.xpath('//td[b="Localização:"]/text()').extract()]
        item["data_contribuinte"] = [item.strip() for item in response.xpath('//td[b="Data de Inclusão do Contribuinte:"]/text()').extract()]
        # item["atividade_principal"] = [item.strip() for item in response.xpath('//table[@id="Table7"]//td[@colspan="5"][1]//text()').extract()]
        item["atividade_secundaria"] = [item.strip() for item in response.xpath('//table[@id="Table7"]//td[@class="style89"]//text()').extract()]
        item["condicao"] = [item.strip() for item in response.xpath('//td[b="Condição:"]/text()').extract()]
        item["forma_pagamento"] = [item.strip() for item in response.xpath('//td[b="Forma de pagamento:"]/text()').extract()]
        item["situacao_vigente"] = [item.strip() for item in response.xpath('//td[b="Situação Cadastral Vigente:"]/text()').extract()]
        item["data_cadastral"] = response.xpath('//td[b="Data desta Situação Cadastral:"]/text()').extract()
        item["endereco_correspondencia"] = [item.strip() for item in response.xpath('//table[@id="Table18"]//td[b="Endereço:"]/text()').extract()]
        item["complemento_correspondencia"] = [item.strip() for item in response.xpath('//table[@id="Table18"]//td[b="Complemento:"]/text()').extract()]
        item["referencia_correspondencia"] = [item.strip() for item in response.xpath('//table[@id="Table18"]//td[b="Referência:"]/text()').extract()]
        item["numero_correspondencia"] = [item.strip() for item in response.xpath('//table[@id="Table18"]//td[b="Número:"]/text()').extract()]
        item["bairro_correspondencia"] = [item.strip() for item in response.xpath('//table[@id="Table18"]//td[b="Bairro:"]/text()').extract()]
        item["cep_correspondencia"] = [item.strip() for item in response.xpath('//table[@id="Table18"]//td[b="CEP:"]/text()').extract()]
        item["municipio_correspondencia"] = [item.strip() for item in response.xpath('//table[@id="Table18"]//td[b="Município:"]/text()').extract()]
        item["uf_correspondencia"] = [item.strip() for item in response.xpath('//table[@id="Table18"]//td[b="UF:"]/text()').extract()]
        return item